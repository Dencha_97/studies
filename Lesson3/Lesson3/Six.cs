﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Six
	{
		/*
		 6*. Реализовать игру “Сапер”. Игрок определяет уровень от 1 до 5.
			Уровень обозначает количество мин на поле, умноженное на 2.
			На нем расставляются мины случайным образом в количестве от 2 до
			10. Отображается поле 5 х 5 в виде стандартной матрицы, где ячейка условный
			знак (например: ‘*’, или ‘’, или ‘o’ не имеет значения).
			Начинается игра. Игрок вводит позицию ячейки.
			Если на этой ячейке не было мины новая попытка.
			Игрок побеждает, когда он откроет не менее 2/3 от безопасных полей.
			Игрок проигрывает, если случайно попадет на 2 мины.
			После каждого хода матрица обновляется (“перерисовывается”), где
			вместо исходного символа в месте очередного выбора, ставится
			символ соответствующий текущему состоянию этой клетки.
			Если игрок указывает на уже открытую ячейку, то вывод сообщения
			на экран, что ячейка уже открыта и запрос повторного ввода ячейки
			для проверки.
			Примечание: В качестве типа матрицы использовать перечисление
			(enum). А его элементы должны соответствовать контексту
			значений ячеек (то есть “не проверена”, “безопасная”, “мина”). Для
			случайного расположение“мин” использовать класс Random (см.
			MSDN)
		 */

		enum MyEnum
		{
			не_проверена,
			безопасная,
			мина
		};

		static public void Method()
		{
			int level;

			char[,] pole = new char[5, 5];
			for (int i = 0; i < pole.GetLength(0); i++)
			{
				for (int j = 0; j < pole.GetLength(1); j++)
				{
					pole[i, j] = '*';
					Console.Write(pole[i, j] + "\t");
				}
				Console.WriteLine();
			}

			do
			{
				Console.Write("Введите уровень (1-5): ");
				level = Int32.Parse(Console.ReadLine());

				switch (level)
				{
					case 1:
						OneLevel();
						break;
					case 2:
						TwoLevel();
						break;
					case 3:
						ThreeLevel();
						break;
					case 4: 
						FourLevel();
						break;
					case 5:
						FiveLevel();
						break;
					default:
						Console.WriteLine("Введен не правильно уровенб, повторите попытку");
						break;
				}
			} while (true);
		}
		static void OneLevel()
		{
			Random random = new Random();
			int r = random.Next(2);
			

			for (int i = 0; i < r; i++)
			{
				
			}
		}
		static void TwoLevel()
		{
			//4
		}
		static void ThreeLevel()
		{
			//6
		}
		static void FourLevel()
		{
			//8
		}
		static void FiveLevel()
		{
			//10
		}
	}
}
