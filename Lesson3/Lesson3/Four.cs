﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Four
	{
        /*
		 4. Изучить исходник bubblesort.cs, демонстрирующий ручную
			сортировку массива алгоритмом типа “Сортировка пузырьком”.
			Модифицировать код таким образом, чтобы сортировка ранжировала
			(упорядочивала) массив не по возрастанию, а по убыванию и лишь с
			той частью массива, т.е. с его подпоследовательностью, которая не
			включает в себя первый (“нулевой”) и последний элементы. (Пример:
			1 9 2 5 7 сортировать надо только 9 2 5)
		 */

        static public void Method()
		{
            Random rnd = new Random();
            int[] mas = new int[10];


            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rnd.Next(1, 10);
            }

            foreach (int i in mas)
            {
                Console.Write(i + ", ");
            }
            Console.WriteLine(" - ДО ");

            /* 
             * СОРТИРОВКА ПУЗЫРЬКОМ
             */

            for (int i = 0; i < mas.Length - 1; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if (mas[i] <= mas[j]) // проверяем "элемент_слева" > "элемент_справа"?
                    {
                        //"меняем" местами меньший правый элемент на больший левый

                        int tmp = mas[i]; //запоминаем "элемент_слева" ШАГ 1
                        mas[i] = mas[j];  //присваиваем к "элементу_слева" значение "элемента_справа" ШАГ 2
                        mas[j] = tmp;     //присваиваем к "элементу_справа" запомненное ранее на шаге 1 значение "элемента_слева" ШАГ 3
                    }
                }
            }

            /*
             END СОРТИРОВКА ПУЗЫРЬКОМ
             */

            foreach (int i in mas)
            {
                if (mas[0] == i)
                    continue;
                if (mas[9] == i)
                    continue;
                Console.Write(i + ", ");
            }
            Console.Write(" - ПОСЛЕ");
        }
	}
}
