﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Five
	{
		/*5. Реализовать программу “Взлом пароля”, методикой Bruteforce	(простой перебор).
			Пользователь сбрасывает пароль, система предлагает ввести от 8 до
			10 знаков интегрального типа long, в случае меньшего или большего
			количества, выдается ошибка. После ввода нового пароля, он его
			забывает. А чтобы войти в систему в бесконечном цикле предлагается
			ввести пароль, пользователь вводит ключевое слово “hack” и
			начинается перебор пароля до успешного совпадения.
		 */

		static public void Method()
		{
			long passwordOne, passwordTwo;
			string key;

			do
			{
				Console.Write("Введите пароль из 8-10 цифр: ");
				passwordOne = Convert.ToInt64(Console.ReadLine());

				if (passwordOne.ToString().Length >= 8 && passwordOne.ToString().Length <= 10)
				{
					Console.WriteLine($"Пароль задан верно!\n Ваш пароль: {passwordOne}");
					break;
				}
				else
				{
					Console.WriteLine("Ошибка, пароль слишком маленький или большой. Введите заново! ");
				}
			} while (true);

			do
			{
				Console.Write("Введите ключевое слово: ");
				key = Console.ReadLine();

				if(key == "hack")
				{
					for(passwordTwo = (long)1E+7; passwordTwo < (long)1E+10; passwordTwo++)
					{
						if (passwordTwo == passwordOne)
						{
							Console.WriteLine($"Пароль найден: {passwordTwo}");
							break;
						}
					}
					break;
				}
			} while (true);
		}
	}
}
