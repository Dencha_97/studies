﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class One
	{
		//1. Реализовать программу “Средний уровень заработной платы”.
		//Вводятся зарплаты 7 сотрудников.Функция принимает массив
		//данных и возвращает на выходе среднее значение заработной платы.

		static public void Method()
		{
			int[] zp = new int[7];
			int allZP = 0; //вся сумарная зп
			int srZP;		//средняя зп

			for(int i = 0; i < zp.Length; i++)
			{
				Console.Write("Введите зарплату {0}-ого сот-ка ", i);
				zp[i] = Convert.ToInt32(Console.ReadLine());
				allZP += zp[i];
			}

			srZP = allZP / 7;

			Console.WriteLine("Среднее значение заработной платы: " + srZP);
		}

	}
}
