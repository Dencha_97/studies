﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Three
	{
		/*
		 3. Программа подсчета произведения. Реализовать с использованием
			функции, в которую может передаваться нефиксированное количество
			параметров. На выходе результат вычисления.
		 */

		static public void Method()
		{
			int[] itemArray = new int[5];

			for(int i=0; i < itemArray.Length; i++)
			{
				Console.Write("Введите {0}-ое число: ", i);
				itemArray[i] = Convert.ToInt32(Console.ReadLine());
			}

			Console.WriteLine("Произведение всех чисел = " + ProizvedenieMethod(itemArray));
		}

		static int ProizvedenieMethod(int[] array)
		{
			int proizvedenie = 1;

			for (int i = 0; i < array.Length; i++)
			{
				proizvedenie *= array[i];
			}
				
			return proizvedenie;
		}
	}
}
