﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
	class Two
	{
		//2. Реализовать программу “Отсев экстремальных оценок”.
		//Спортивное соревнование.Система предлагает выставить оценки.
		//Судьи выставляют шесть оценок. Исключаются 1 максимальная и 1
		//минимальная оценки. Выводится итоговая последовательность.

		static public void Method()
		{
			int[] appraisal = new int[6];


			for (int i = 0; i < appraisal.Length; i++)
			{
				Console.Write("Введите {0}-ую оценку: ", i);
				appraisal[i] = Convert.ToInt32(Console.ReadLine());
			}
			int max = appraisal[0];
			int min = appraisal[0];

			//max
			for (int i = 0; i < appraisal.Length - 1; i++)
			{
				for (int j = i + 1; j < appraisal.Length; j++)
				{
					//не правильно считает
					if (appraisal[i] > appraisal[j])
					{
						max = appraisal[i];
						appraisal[i] = appraisal[j];
						appraisal[j] = max;
					}
				}
			}

			//min
			for (int i = 0; i < appraisal.Length - 1; i++)
			{
				for (int j = i + 1; j < appraisal.Length; j++)
				{
					if (appraisal[i] < appraisal[j])
					{
						min = appraisal[i];
						appraisal[i] = appraisal[j];
						appraisal[j] = min;
					}
				}
			}

			//исключить max, min
			for (int i = 0; i < appraisal.Length; i++)
			{
				if (appraisal[i] == max)
					continue;
				else if (appraisal[i] == min)
					continue;
				else
					Console.Write(appraisal[i] + ", ");
			}
		}


	}
}
