﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4
{
	class Program
	{
		static void Main(string[] args)
		{ 
			//Lesson4.One.Result.Method(); //+
			//Lesson4.Two.Result.Method(); //+ (постараться реализовать через метод)
			Lesson4.Three.Result.Method(); //-
			//Lesson4.Four.Result.Method(); //-
			//Lesson4.Five.Result.Method(); //-
			Console.ReadKey();
		}
	}
}
