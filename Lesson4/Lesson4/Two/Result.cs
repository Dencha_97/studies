﻿using ImTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Two
{
	/*
	 2. Создать программу, описывающую некоторых сотрудников.
		Позволяет вычислять среднюю зарплату, а также выводить
		максимальное и минимальное значение.
		Примечание: классы “Сотрудник”, Поля: имя, фамилия, возраст,
		должность, зарплата. Класс “Сотрудники” включает массив из
		объектов класса “Сотрудник”. Предусмотреть возможность
		добавления сотрудников в “Сотрудники”.
	 */
	class Result
	{
		public static void Method()
		{			
			string name;
			string lastname;
			int age;
			string position;
			int salary;
			int kolvo;

			Staff staffOb = new Staff();
			Employee employee = new Employee();
			//Console.Write("Количество сотрудников: ");
			//kolvo = Convert.ToInt32(Console.ReadLine());
			
			Employee[] emp = new Employee[2];
			emp[0] = new Employee("Den", "Zh", 23, "stud", 4);
			emp[1] = new Employee("Alex", "Zher", 30, "alfa", 6);

			//Staff sOb = new Staff(kolvo);
			Staff[] staff = new Staff[2];
			staff[0] = emp[0];
			staff[1] = emp[1];

			//for (int i = 0; i < 2; i++)
			//{
			//	Console.WriteLine(i);
			//	Console.Write("Имя: ");
			//	name = Console.ReadLine();
			//	Console.Write("Фамилия: ");
			//	lastname = Console.ReadLine();
			//	Console.Write("Возраст: ");
			//	age = Convert.ToInt32(Console.ReadLine());
			//	Console.Write("Должность: ");
			//	position = Console.ReadLine();
			//	Console.Write("Зарплата: ");
			//	salary = Convert.ToInt32(Console.ReadLine());

			//	staff[i] = new Employee(name, lastname, age, position, salary);
			//}

			foreach (var item in staff)
			{
				staffOb.Show((Employee)item);
			}

			Console.WriteLine();

			//foreach (var item in staff)
			//{ 
			//	Console.WriteLine("Средняя зарплата: " + staffOb.AsrSalary((Employee)item));
			//}

			int sum = 0;
			int count = 0;
			int max = emp[0].Salary;
			int min = emp[0].Salary;
			for (int i = 0; i < staff.Length; i++)
			{
				sum += emp[i].Salary;
				count++;

				if (emp[i].Salary > max)
					max = emp[i].Salary;

				if (emp[i].Salary < min)
					min = emp[i].Salary;
			}

			double asr = (double) (sum / count);
			Console.WriteLine("Средняя зарплата: " + asr);
			Console.WriteLine("Максимальная зарплата: " + max);
			Console.WriteLine("Минимальная зарплата: " + min);

		}
	}
}