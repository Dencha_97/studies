﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1
{
	enum Time 
	{
		year = 12,
		week = 4,
		day = 22,
		hour = 8*22,
		minutes = 60 * 8 * 22
	}

	class Program
	{
		static void Main(string[] args)
		{
			//1.Определить размер в байтах типа decimal и вывести на экран.
			Console.WriteLine("1.Определить размер в байтах типа decimal и вывести на экран.");
			Console.WriteLine("Размер decimal равен " + sizeof(decimal) + " Байт");
			Console.WriteLine();

			//2.Реализовать сумматор.На вход поддается два числа(операнда). На выходе выводится результат сложения.
			Console.WriteLine("2.Реализовать сумматор.На вход поддается два числа(операнда). На выходе выводится результат сложения.");

			int n1, n2;

			Console.Write("Введение числа №1: ");
			n1 = Convert.ToInt32(Console.ReadLine());
			Console.Write("Введение числа №2: ");
			n2 = Convert.ToInt32(Console.ReadLine());

			int result = n1 + n2;

			Console.WriteLine("Сумма двух чисел равна: {0}", result);
			Console.WriteLine();

			//3.Написать программу “Анкета”. Последовательно задаются вопросы(имя, фамилия, возраст, город рождения, рост, вес).
			//В результате выводится вся информация в одну строчку.
			Console.WriteLine("3.Написать программу “Анкета”. Последовательно задаются вопросы(имя, фамилия, возраст, город рождения, рост, вес). " +
			"В результате выводится вся информация в одну строчку.");
			string name, surname, city;
			int age = 0;
			double height = 0.00, weight = 0.00;

			Console.Write("Ваше имя: ");
			name = Console.ReadLine();
			Console.Write("Ваша фамилия ");
			surname = Console.ReadLine();
			Console.Write("Ваш возраст: ");
			age = Convert.ToInt32(Console.ReadLine());
			Console.Write("Город рождения: ");
			city = Console.ReadLine();
			Console.Write("Ваш рост: ");
			height = Convert.ToDouble(Console.ReadLine());
			Console.Write("Ваш вес: ");
			weight = Convert.ToDouble(Console.ReadLine());

			Console.WriteLine();

			//4.Написать программу, которая определяет заработок пользователя за год, неделю, сутки, час, минуту. 
			//В качестве входного параметра пользователь должен ввести, сколько он зарабатывает в месяц. В консольном окне должно отобразится.
			int salary = 0;
			Console.Write("Ваша зарплата в месяц: ");
			salary = Convert.ToInt32(Console.ReadLine());

			int yearVar = salary * (int)Time.year;
			Console.Write("Зарплата в год: " + yearVar + " руб.\n");
			int weekVar = salary / (int)Time.week;
			Console.Write("Зарплата в неделю: " + weekVar + " руб.\n");
			int hourVar = salary / (int)Time.hour;
			Console.Write("Зарплата в час: " + hourVar + " руб.\n");
			int minutesVar = salary / (int)Time.minutes;
			Console.Write("Зарплата в минуту: " + minutesVar + " руб.\n");
			Console.WriteLine();

			//5. Написать программу, которая вычисляет корни квадратного уравнения. Пользователь вводит параметры a, b, c.
			double a, b, c;

			Console.Write("Введите a: ");
			a = Convert.ToDouble(Console.ReadLine());
			Console.Write("Введите b: ");
			b = Convert.ToDouble(Console.ReadLine());
			Console.Write("Введите c: ");
			c = Convert.ToDouble(Console.ReadLine());

			double D = Math.Sqrt(b) - 4 * a * c;
			Console.WriteLine("Дискриминант равен: " + D);

			double x1 = (-b + Math.Sqrt(D)) / (2 * a);
			double x2 = (-b - Math.Sqrt(D)) / (2 * a);

			Console.WriteLine("Корень х1: " + x1);
			Console.WriteLine("Корень х2: " + x2);

			Console.ReadKey();
		}
	}
}
