﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class Two
	{
		//2.Реализовать программу Текстовый квест “Найди клад!”
		//Игрок находится на ОПУШКЕ перед ним две дороги. Он выбирает одну из них, одна ведет в болото, другая в сад к заброшенному дому.
		//Если игрок оказывается в САДУ рядом с домом, у него снова выбор пойти дальше или зайти в дом. (Дорога ведет в болото) Оказавшись в
		//ДОМЕ, перед ним сундук и шкатулка.В одном клад, в другом ловушка.
		//Примечание: реализовать каждое состояние в виде отдельного метода(функции).В Main лишь вызов исходного состояния(функции).
		public static void Method()
		{
			string direction;

			Console.Write("Введите в какую сторону пойдете (налево/направо): ");
			direction = Console.ReadLine();

			DirectionMethod(direction);
		}

		static void DirectionMethod(string direction)
		{

			if (direction == "налево")
			{
				Console.WriteLine("Лошарик, Дорога ведет в болото");
			}
			else if (direction == "направо")
			{
				Console.WriteLine("Красавчик, Дорога ведет в сад");
				Console.Write("Снова выбор, куда пойдете (домой или дальше): ");
				direction = Console.ReadLine();
				Right(direction);
			}
		}

		static void Right(string direction)
		{
			if (direction == "домой")
			{
				Console.Write("Ты вошел в дом, что выберешь (шкатулка или сундук): ");
				direction = Console.ReadLine();
				Home(direction);
			}
			else if (direction == "дальше")
			{
				Console.WriteLine("Ну иди дальше...");
			}
		}

		static void Home(string direction)
		{
			if (direction == "шкатулка")
			{
				Console.WriteLine("Повезло, достался клад тебе)");
			}
			else if (direction == "сундук")
			{
				Console.WriteLine("ХА, ловушка: Викой в глаз или ...");
			}
		}
	}
	
}
