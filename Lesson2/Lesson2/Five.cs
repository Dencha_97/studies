﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class Five
	{
		//5 *.Реализовать программу, которая определяет текущее время и в зависимости от этого диапазона выводит одно из сообщений:
		//“Доброе утро!”, “Добрый день!”, “Добрый вечер!”, “Доброй ночи!”
		//Кроме того, выводит текущее время.
		//Примечание: Необходимо ознакомиться с соответствующим функционалом DateTime, найти у него необходимый метод(функцию).
		//http://msdn.microsoft.com/ru-ru/library/03ybds8y.aspx
		//Использовать логические операции.
		public static void Method()
		{
			var dataTime = DateTime.Now.TimeOfDay;

			if (TimeSpan.Parse("04:00") <= dataTime && dataTime <= TimeSpan.Parse("11:59"))
				Console.WriteLine("Доброе утро! \n сейчас " + dataTime);
			if (TimeSpan.Parse("12:00") <= dataTime && dataTime <= TimeSpan.Parse("17:59"))
				Console.WriteLine("Добрый день! \n сейчас " + dataTime);
			if (TimeSpan.Parse("18:00") <= dataTime && dataTime <= TimeSpan.Parse("21:59"))
				Console.WriteLine("Добрый вечер! \n сейчас " + dataTime);
			if (TimeSpan.Parse("22:00") <= dataTime && dataTime <= TimeSpan.Parse("03:59"))
				Console.WriteLine("Доброй ночи! \n сейчас " + dataTime);
		}
	}
}
