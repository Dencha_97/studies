﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson2
{
	class Four
	{
		//4.Реализовать программу “Прогноз погоды на завтра”.
		//Пользователь вводит название города и получает температуру воздуха, воды, скорость ветра, давление или оповещение, что метеоданных по этому городу нет.
		static public void Method()
		{
			string city;
			Console.Write("Введите город: ");
			city = Console.ReadLine();

			switch(city)
			{
				case "москва":
					Climat();
					break;
				case "рига":
					Climat();
					break;
				case "уфа":
					Climat();
					break;
				case "тула":
					Climat();
					break;
				default:
					Console.Write("Такого города нет");
					break;
			}
			
		}

		static void Climat()
		{
			Random r = new Random();
			Console.WriteLine("Температура воздуха: " + r.Next(-10, 20));
			Console.WriteLine("Температура воды: " + r.Next(-10, 20));
			Console.WriteLine("Скорость ветра: " + r.Next(-10, 20));
			Console.WriteLine("Давление: " + r.Next(-10, 20));
		}
	}
}
